[INSTRUCTIONS]
Before you start please paste the two folders included ("savedSetups" & "previewPics") in the following location:
C:\Users\<yourNamehere>\Documents\maya\projects

The upper line of buttons is just basic arnold lights and functions.

Underneath that you have the premade setups, those are the txt files in your "savedSetups" folder. With the image for the icon coming from the "previewPics" folder.
The line after that is bifunctional. If you select a couple of lights, enter a name in the textField and then click "Save Custom Setup", the setup will be saved in
the "savedSetups" folder and it will immediately create an empty button with the name of the setup.

Then you can leave the name in the field and click "Create Thumbnail" which will spawn a preview setup (sphere, camera and infini) and render out the thumbnail image,
giving your empty button an image.

If you have a button you want to remake, just enter the name of that setup back in the textfield, hide any meshes in your scene, spawn the light setup you want to
render again and click Create Thumbnail again. If you leave the name there, adjust some sliders and click the button again your thumbnail will update in realtime
(with slight delay for the rendertime of course)

The bottom part is a light lister setup that allows you to control your lights.

There are error message pop ups for things that are not allowed (light portals without skydomes, empty mesh lights etc...)

If you wanna render out a thumbnail for your light setup don't forget to hide any meshes already present in your scene


[PERMISSIONS]
This script can be used for anything, anywhere. The only restriction is that you do not share this script without mailing me for permission (thoranslock@hotmail.com).
If you mail me with the reasons you want to share it and with whom, your request might be approved after which you can share the script with THOSE PEOPLE ONLY.


[UPDATES]
If you have any suggestions for extra functions or updates please feel free to mail me. (thoranslock@hotmail.com)
Upon showing of proof you have bought a previous version of DynaLight you can get any update for free of course.