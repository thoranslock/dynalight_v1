import maya.cmds as cmds
import mtoa.core as core
import pymel.core as pmc
import mtoa.utils as mutils
import mtoa.ui.arnoldmenu as arnoldmenu
from mtoa.cmds.arnoldRender import arnoldRender
import functools
import json
import os
from collections import OrderedDict
import re
import maya.mel as mel
import maya.app.general.createImageFormats as createImageFormats

dicExists = False
# lightListerExists = False

class DynaLight:
    def __init__(self, *args):
        # USER INTERFACE SETUP

        # Variables
        self.setups_dir = 'savedSetups'
        self.saveLocation = self.create_custom_save_location(self.setups_dir)
        self.thumbnails_dir = 'previewPics'
        self.thumbnailsLocation = self.create_custom_save_location(self.thumbnails_dir)

        # MAKE WINDOW
        self.windowWidth = 820
        self.windowHeight = 500
        self.previewSize = 260
        self.counter = 0
        self.windowID = "windowUI"

        if cmds.window(self.windowID, exists=True):  # simple line deletes the previous window
            cmds.deleteUI(self.windowID)
        self.counter += 1
        self.win = cmds.window(self.windowID, height=self.windowHeight, width=self.windowWidth,
                               title="DynaLight", sizeable=False)

        # STANDARD LIGHT BUTTONS
        cmds.columnLayout(adj=True)
        cmds.separator(height=2)
        self.numCols = 22
        cmds.rowLayout(numberOfColumns=self.numCols)
        cmds.separator(w=5, style='none')
        cmds.text(label="Create Lights: ", font="boldLabelFont")
        cmds.iconTextButton(image1='AreaLightShelf.png', label="areaLight", command=self.createAreaLight)
        cmds.iconTextButton(image1='MeshLightShelf.png', label="meshLight", command=self.createMeshLight)
        cmds.iconTextButton(image1='PhotometricLightShelf.png', label="photometricLight",
                            command=self.createPhotoMetricLight)
        cmds.iconTextButton(image1='SkyDomeLightShelf.png', label="skyDomeLight", command=self.createSkyDome)
        cmds.iconTextButton(image1='LightPortalShelf.png', label="lightPortal", command=self.createLightPortal)
        cmds.iconTextButton(image1='PhysicalSkyShelf.png', label="physSky", command=self.createPhysicalSky)
        cmds.separator(w=5, style='none')
        cmds.iconTextButton(image1='RenderViewShelf.png', label="renderView", command=self.openRenderView)
        cmds.iconTextButton(image1='RenderShelf.png', label="renderOut", command=self.renderInRenderView)
        cmds.separator(w=1, style='none')

        cmds.button(label="Aim-Object", command=self.aimLightsAtObject, width=70)
        cmds.separator(w=1, style='none')
        cmds.button(label="Aim-Center", command=self.aimLightsAtCenter, width=70)
        cmds.separator(w=2, style='none')
        cmds.checkBox(label='Scene Lock', al='left', ofc=self.isMovable,
                      onc=self.isLocked, value=True, w=83)
        cmds.separator(w=2, style='none')
        cmds.button(label="Delete All", command=self.deleteAllLights, width=90)
        cmds.separator(w=1, style='none')
        cmds.button(label="ShaderCon", command=self.shaderConversion, width=90)
        cmds.separator(w=1, style='none')
        cmds.setParent('..')

        # PRESET BUTTONS
        cmds.separator(h=10)
        self.presetsID = "presetButtons"
        self.myScroll = cmds.scrollLayout(self.presetsID, height=550,
                                          verticalScrollBarThickness=16,
                                          horizontalScrollBarThickness=1, width=820)
        self.myRows = cmds.rowColumnLayout(numberOfColumns=6, parent=self.myScroll)
        self.presetButtons()

        self.presetCols = 3
        cmds.setParent(self.windowID)
        cmds.rowLayout(numberOfColumns=1)
        cmds.separator(h=10)

        # SAVE SETUPS
        cmds.setParent(self.windowID)
        cmds.rowLayout(numberOfColumns=10)
        cmds.separator(w=5, style='none')
        cmds.text(label="PRESET NAME: ", width=100, align='left', font="boldLabelFont")
        self.presetName = cmds.textField(text='Custom Preset 1', width=340)
        cmds.separator(w=5, style='none')
        cmds.button(label="Create Thumbnail", command=functools.partial(self.spawnPreviewSetup,
                                                                        self.presetName), width=115)
        cmds.separator(w=4, style='none')
        cmds.button(label="Save Custom Setup", command=functools.partial(self.saveSetups,
                                                                         self.presetName), width=115)
        cmds.separator(w=4, style='none')
        cmds.button(label="Load Custom Setup", command=self.loadAnyPreset, width=115)
        cmds.separator(w=5, style='none')
        cmds.setParent('..')

        # LIGHT SLIDERS
        cmds.separator(h=10)
        self.createLightTitles()
        cmds.setParent(self.titlesID)
        self.lightController()
        cmds.setParent('..')
        cmds.separator(h=5, style='none')

    # Function that shows UI
    def showUI(self, *args):
        print "UI RUN (test)"
        cmds.showWindow(self.win)

    # Function that loops through the savedSetups folder and creates a button for each text file
    def presetButtons(self, *args):
        amountOfFiles = 0
        index = []

        if cmds.rowColumnLayout(self.myRows, exists=True):
            cmds.deleteUI(self.myRows, layout=True)

        for roots, dirs, files in os.walk(self.saveLocation):
            for file in files:
                if file.endswith('.txt'):
                    amountOfFiles += 1
                    # print "FILE: %s (test)" % file

        for j in range(amountOfFiles):
            newIndex = 2+(j*3)
            index.append(newIndex)

        self.myRows = cmds.rowColumnLayout(numberOfColumns=6, parent=self.myScroll)
        for i in range(amountOfFiles):
            removal = ".txt"
            baseName = files[i].replace(removal, "")

            cmds.separator(w=5, style='none')

            # cmds.button(label="%s" % files[i], w=260, h=260)
            cmds.iconTextButton(label=baseName, ann=baseName, style="iconAndTextCentered", fn="boldLabelFont",
                                image=self.proj_dir + '/previewPics/%s_pp.jpg' % baseName,
                                command=functools.partial(self.load_setups, files[i]),
                                width=self.previewSize, height=self.previewSize)

            if i in index:
                cmds.separator(w=5, style='none')
                cmds.separator(h=5, style='none')
                cmds.separator(h=5, style='none')
                cmds.separator(h=5, style='none')
                cmds.separator(h=5, style='none')
                cmds.separator(h=5, style='none')

    # Function that creates titles for light controller
    def createLightTitles(self, *args):
        # print "TITLES CREATED (test)"
        self.titlesID = "titlesID"
        if cmds.rowLayout(self.titlesID, exists=True):
            cmds.deleteUI(self.titlesID, layout=True)
        self.controlsNum = 8
        cmds.rowLayout(self.titlesID, numberOfColumns=self.controlsNum)
        cmds.separator(w=3, style='none')
        cmds.text(label="Visible", w=72, align='left', font="boldLabelFont")
        cmds.text(label="Light Name", w=190, align='left', font="boldLabelFont")
        cmds.text(label="Color", w=210, align='left', font="boldLabelFont")
        cmds.text(label="Intensity", w=200, align='left', font="boldLabelFont")
        cmds.text(label="Exposure", w=122, align='left', font="boldLabelFont")
        cmds.separator(w=3, style='none')
        cmds.separator(h=5, style='none')

    # Function that creates the sliders to control the lights
    def lightController(self, *args):
        cmds.setParent(self.windowID)

        allLights = cmds.ls(type=['aiAreaLight', 'aiSkyDomeLight', 'aiPhotometricLight', 'aiPhysicalSky'])
        lightsNum = len(allLights)

        scrollID = "scroll"

        if cmds.scrollLayout(scrollID, exists=True):
            cmds.deleteUI(scrollID, control=True, layout=False)
            print "LIGHTS ID DELETED"

        scrollLayout = cmds.scrollLayout(scrollID, height=200,
                                         verticalScrollBarThickness=16,
                                         horizontalScrollBarThickness=1)
        cmds.rowColumnLayout(numberOfColumns=7)

        for i in range(lightsNum):
            removal = "Shape"
            lightName = "%s" % allLights[i].replace(removal, "")

            if "LightPortal" not in lightName and "Physical" not in lightName:
                cmds.separator(w=12, style='none')
                cmds.checkBox(label='', onc=functools.partial(self.turnOn, allLights[i]),
                              ofc=functools.partial(self.turnOff, allLights[i]), value=1, w=20)
                cmds.text(label=lightName, w=155, align='center')
                cmds.attrColorSliderGrp(label='', columnWidth=[(1, 0), (2, 50), (3, 125), (4, 10), (5, 0)],
                                        attribute='%s.color' % allLights[i])
                cmds.attrFieldSliderGrp(label='', min=0.0, max=150.0,
                                        columnWidth=[(1, 0), (2, 60), (3, 125), (4, 10), (5, 0)],
                                        at='%s.intensity' % allLights[i])
                cmds.attrFieldSliderGrp(label='', min=0.0, max=20.0,
                                        columnWidth=[(1, 0), (2, 50), (3, 125), (4, 10), (5, 0)],
                                        at='%s.exposure' % allLights[i])
                cmds.separator(w=3, style='none')

        value = cmds.scrollLayout(scrollLayout, query=True, scrollAreaValue=True)
        top = value[0]
        left = value[1]

    # Function that creates Area Light
    def createAreaLight(self, *args):
        mutils.createLocator("aiAreaLight", asLight=True)
        self.lightController()

    # Function that creates SkyDomeLight
    def createSkyDome(self, *args):
        mutils.createLocator("aiSkyDomeLight", asLight=True)
        self.lightController()

    # Function that creates Mesh Light
    def createMeshLight(self, *args):
        selectedMeshes = cmds.ls()
        if len(selectedMeshes) > 0:
            mutils.createMeshLight()

        else:
            print "Please select at least one mesh first."

        self.lightController()

    # Function that creates Photometric Light
    def createPhotoMetricLight(self, *args):
        mutils.createLocator("aiPhotometricLight", asLight=True)
        self.lightController()

    # Function that creates Light Portal
    def createLightPortal(self, *args):
        amountOfSkyDomes = len(cmds.ls(type="aiSkyDomeLight"))
        print amountOfSkyDomes

        if amountOfSkyDomes > 0:
            arnoldmenu.doCreateLightPortal()
            print "YES"

        else:
            cmds.confirmDialog(title='Error', message="Please make sure there is a SkyDome present first",
                               button=['CONFIRM'])

        self.lightController()

    # Function that creates Physical Sky
    def createPhysicalSky(self, *args):
        arnoldmenu.doCreatePhysicalSky()
        self.lightController()

    # Function that opens Arnold Render View
    def openRenderView(self, *args):
        core.createOptions()
        cmds.arnoldRenderView(mode="open")

    # Function that opens Arnold Render View and launches a render
    def renderInRenderView(self, *args):
        if core.ACTIVE_CAMERA != None:
            cmds.arnoldRenderView(cam=core.ACTIVE_CAMERA)

        cmds.arnoldRenderView(mode="open")
        core.createOptions()
        cmds.arnoldRenderView()

    # Function that converts a standard Phong shader to an aiStandardSurface shader with same base color (primitive)
    def shaderConversion(self, *args):
        shaders = pmc.selected()
        for shader in shaders:
            shaderNames = shader.getName()
            print shaderNames
            aiShader = pmc.rendering.shadingNode("aiStandardSurface", asShader=True, name="ai_%s" % shaderNames)
            baseColor = shader.color.get()
            aiShader.baseColor.set(baseColor)

    # AIM CONSTRAINS LIGHTS TO OBJECT
    def aimLightsAtObject(self, *args):
        selectionList = cmds.ls(orderedSelection=True)

        # this is to check if at least two objects are selected
        if len(selectionList) >= 2:

            print 'Selected items: %s' % (selectionList)

            targetName = selectionList[0]  # to keep track of first object in list

            selectionList.remove(targetName)  # to remove from selection list (deselect I believe)

            for objectName in selectionList:
                print 'Constraining %s to %s' % (objectName, targetName)
                # just to dubbelcheck if we are actually iterating over correct objects

                cmds.aimConstraint(targetName, objectName, aimVector=[0, 0, 1], maintainOffset=False,
                                   offset=[0, 180, 0], worldUpType="scene")

        else:
            # if requirement of at least two selected objects isn't met
            cmds.confirmDialog(title='Error', message="Please select a light and one or more objects first",
                               button=['CONFIRM'])

    # Function that aims all the lights at a created CenterOfScene_LOC
    def aimLightsAtCenter(self, *args):
        selectionList = cmds.ls(orderedSelection=True, type='transform')

        if len(selectionList) > 0:

            if not cmds.objExists("CenterOfScene_LOC"):
                centerOfSceneLocator = cmds.spaceLocator(absolute=True, position=[0, 0, 0], name="CenterOfScene_LOC")
                cmds.setAttr("CenterOfScene_LOC.tx", lock=True, keyable=False, channelBox=False)
                cmds.setAttr("CenterOfScene_LOC.ty", lock=True, keyable=False, channelBox=False)
                cmds.setAttr("CenterOfScene_LOC.tz", lock=True, keyable=False, channelBox=False)

            for objectName in selectionList:
                print 'Constraining %s to center of scene' % objectName

                cmds.aimConstraint("CenterOfScene_LOC", objectName, aimVector=[0, 0, 1], maintainOffset=False,
                                   offset=[0, 180, 0], worldUpType="scene")

        else:
            cmds.confirmDialog(title='Error', message="Please select some lights first",
                               button=['CONFIRM'])

    # Function that unlocks the CenterOfScene_LOC
    def isMovable(self, *args):
        cmds.setAttr("CenterOfScene_LOC.tx", lock=False, keyable=True, channelBox=True)
        cmds.setAttr("CenterOfScene_LOC.ty", lock=False, keyable=True, channelBox=True)
        cmds.setAttr("CenterOfScene_LOC.tz", lock=False, keyable=True, channelBox=True)

    # Function that locks the CenterOfScene_LOC
    def isLocked(self, *args):
        cmds.setAttr("CenterOfScene_LOC.tx", lock=True, keyable=False, channelBox=False)
        cmds.setAttr("CenterOfScene_LOC.ty", lock=True, keyable=False, channelBox=False)
        cmds.setAttr("CenterOfScene_LOC.tz", lock=True, keyable=False, channelBox=False)

    # Function that deletes all the lights in the scene
    def deleteAllLights(self, *args):
        allLights = cmds.ls(type=['aiAreaLight', 'aiSkyDomeLight', 'aiPhotometricLight', 'aiMeshLight',
                                  'aiLightPortal', 'aiPhysicalSky'])
        lightsNum = len(allLights)
        for i in range(lightsNum):
            cmds.delete(allLights[i])
            scrollID = "scroll"

            if cmds.scrollLayout(scrollID, exists=True):
                cmds.deleteUI(scrollID, control=True, layout=False)
                print "LIGHTS ID DELETED"

        allGroups = cmds.ls('*_lightGroup')
        cmds.delete(allGroups)

        # THIS FUNCTION DELETES THE EMPTY TRANSFORMS IN THE SCENE, TRIED FOR HOURS TO FIX THIS MYSELF
        # COULD NOT FIGURE IT OUT HOW TO SCRIPT IT SO COPIED THIS SMALL PART FROM:
        # https://polycount.com/discussion/71520/maya-script-delete-empty-groups
        transforms = cmds.ls(type='transform')
        deleteList = []
        for tran in transforms:
            if cmds.nodeType(tran) == 'transform':
                children = cmds.listRelatives(tran, c=True)
                if children == None:
                    print '%s, has no children' % (tran)
                    deleteList.append(tran)

        if len(deleteList) > 0:
            cmds.delete(deleteList)
        # END OF COPIED PART

        self.lightController()

    # Function that turns off visibility for the selected light
    def turnOff(self, pLight, *args):
        cmds.setAttr(pLight + '.visibility', 0)

    # Function that turns on visibility for the selected light
    def turnOn(self, pLight, *args):
        cmds.setAttr(pLight + '.visibility', 1)

    # Function to load any setup you want that (in different folders for example)
    def loadAnyPreset(self, *args):
        # print "Load Any Preset (test)"
        removal = "%s/" % self.saveLocation

        presetLoc = cmds.fileDialog2(fm=4)

        for i in range(len(presetLoc)):
            presetLocation = "%s" % presetLoc[i]
            fileName = presetLocation.replace(removal, "")
            self.load_setups(fileName)
            print "PRESET LOCATION: %s(test)" % presetLocation
            print "FILE NAME: %s(test)" % fileName

    # Function that oversees the saving of the setups
    def saveSetups(self, pSetupName, *args):
        setupName = cmds.textField(pSetupName, query=True, text=True)
        self.getSetupAttributes()
        self.create_custom_save_location(self.setups_dir)
        self.save_into_file(setupName)
        self.presetButtons()
        self.lightController()

    # Function to get every relevant attribute of the selected lights and put them in a dictionary
    def getSetupAttributes(self, *args):
        global dicExists
        selectedShapes = cmds.ls(sl=True, dag=True, type='shape')
        selectedLights = cmds.ls(selection=True, type='transform')

        amountOfLights = len(selectedLights)

        # LOOP OVER EACH LIGHT SELECTED
        if len(selectedLights) > 0 or len(selectedShapes) > 0:
            for i in range(amountOfLights):
                nodeType = cmds.nodeType(selectedShapes[i])
                lightType = nodeType

                if lightType == 'aiAreaLight':  # GET ALL THE RELEVANT ATTRIBUTES FOR AN AREA LIGHT
                    vis = cmds.getAttr("%s.visibility" % selectedLights[i])
                    tx = cmds.getAttr("%s.translateX" % selectedLights[i])
                    ty = cmds.getAttr("%s.translateY" % selectedLights[i])
                    tz = cmds.getAttr("%s.translateZ" % selectedLights[i])
                    # print "Translations for object %s are X: %s, Y: %s, Z: %s" % (i, tx, ty, tz)

                    rx = cmds.getAttr("%s.rotateX" % selectedLights[i])
                    ry = cmds.getAttr("%s.rotateY" % selectedLights[i])
                    rz = cmds.getAttr("%s.rotateZ" % selectedLights[i])
                    # print "Rotations for object %s are X: %s, Y: %s, Z: %s" % (i, rx, ry, rz)

                    sx = cmds.getAttr("%s.scaleX" % selectedLights[i])
                    sy = cmds.getAttr("%s.scaleY" % selectedLights[i])
                    sz = cmds.getAttr("%s.scaleZ" % selectedLights[i])
                    # print "Scales for object %s are X: %s, Y: %s, Z: %s" % (i, sx, sy, sz)

                    intensity = cmds.getAttr("%s.intensity" % selectedLights[i])
                    exposure = cmds.getAttr("%s.exposure" % selectedLights[i])
                    colR = cmds.getAttr("%s.colorR" % selectedLights[i])
                    colG = cmds.getAttr("%s.colorG" % selectedLights[i])
                    colB = cmds.getAttr("%s.colorB" % selectedLights[i])
                    softEdge = cmds.getAttr("%s.aiSoftEdge" % selectedLights[i])
                    spread = cmds.getAttr("%s.aiSpread" % selectedLights[i])
                    roundness = cmds.getAttr("%s.aiRoundness" % selectedLights[i])

                    if i == 0:
                        self.setup_dic = {"numOfLights": amountOfLights,
                                          "lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                          "lightType_%s" % (i + 1): lightType,
                                          "visibility_%s" % (i + 1): vis,
                                          "translateX_%s" % (i + 1): tx,
                                          "translateY_%s" % (i + 1): ty,
                                          "translateZ_%s" % (i + 1): tz,
                                          "rotateX_%s" % (i + 1): rx,
                                          "rotateY_%s" % (i + 1): ry,
                                          "rotateZ_%s" % (i + 1): rz,
                                          "scaleX_%s" % (i + 1): sx,
                                          "scaleY_%s" % (i + 1): sy,
                                          "scaleZ_%s" % (i + 1): sz,
                                          "intensity_%s" % (i + 1): intensity,
                                          "exposure_%s" % (i + 1): exposure,
                                          "redColor_%s" % (i + 1): colR,
                                          "greenColor_%s" % (i + 1): colG,
                                          "blueColor_%s" % (i + 1): colB,
                                          "softEdge_%s" % (i + 1): softEdge,
                                          "spread_%s" % (i + 1): spread,
                                          "roundness_%s" % (i + 1): roundness}
                        # print "CREATED DICTIONARY (test)"
                        dicExists = True

                    else:
                        self.setup_dic.update({"lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                               "lightType_%s" % (i + 1): lightType,
                                               "visibility_%s" % (i + 1): vis,
                                               "translateX_%s" % (i + 1): tx,
                                               "translateY_%s" % (i + 1): ty,
                                               "translateZ_%s" % (i + 1): tz,
                                               "rotateX_%s" % (i + 1): rx,
                                               "rotateY_%s" % (i + 1): ry,
                                               "rotateZ_%s" % (i + 1): rz,
                                               "scaleX_%s" % (i + 1): sx,
                                               "scaleY_%s" % (i + 1): sy,
                                               "scaleZ_%s" % (i + 1): sz,
                                               "intensity_%s" % (i + 1): intensity,
                                               "exposure_%s" % (i + 1): exposure,
                                               "redColor_%s" % (i + 1): colR,
                                               "greenColor_%s" % (i + 1): colG,
                                               "blueColor_%s" % (i + 1): colB,
                                               "softEdge_%s" % (i + 1): softEdge,
                                               "spread_%s" % (i + 1): spread,
                                               "roundness_%s" % (i + 1): roundness})
                        # print "DIC UPDATED (test)"

                if lightType == 'aiMeshLight':  # GET ALL THE RELEVANT ATTRIBUTES FOR A MESH LIGHT
                    vis = cmds.getAttr("%s.visibility" % selectedLights[i])
                    showMesh = cmds.getAttr("%s.showOriginalMesh" % selectedLights[i])
                    lightVis = cmds.getAttr("%s.lightVisible" % selectedLights[i])
                    tx = cmds.getAttr("%s.translateX" % selectedLights[i])
                    ty = cmds.getAttr("%s.translateY" % selectedLights[i])
                    tz = cmds.getAttr("%s.translateZ" % selectedLights[i])
                    # print "Translations for object %s are X: %s, Y: %s, Z: %s" % (i, tx, ty, tz)

                    rx = cmds.getAttr("%s.rotateX" % selectedLights[i])
                    ry = cmds.getAttr("%s.rotateY" % selectedLights[i])
                    rz = cmds.getAttr("%s.rotateZ" % selectedLights[i])
                    # print "Rotations for object %s are X: %s, Y: %s, Z: %s" % (i, rx, ry, rz)

                    sx = cmds.getAttr("%s.scaleX" % selectedLights[i])
                    sy = cmds.getAttr("%s.scaleY" % selectedLights[i])
                    sz = cmds.getAttr("%s.scaleZ" % selectedLights[i])
                    # print "Scales for object %s are X: %s, Y: %s, Z: %s" % (i, sx, sy, sz)

                    intensity = cmds.getAttr("%s.intensity" % selectedLights[i])
                    exposure = cmds.getAttr("%s.aiExposure" % selectedLights[i])
                    colR = cmds.getAttr("%s.colorR" % selectedLights[i])
                    colG = cmds.getAttr("%s.colorG" % selectedLights[i])
                    colB = cmds.getAttr("%s.colorB" % selectedLights[i])

                    if i == 0:
                        self.setup_dic = {"numOfLights": amountOfLights,
                                          "lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                          "lightType_%s" % (i + 1): lightType,
                                          "visibility_%s" % (i + 1): vis,
                                          "mesh_visibility_%s" % (i + 1): showMesh,
                                          "light_visibility_%s" % (i + 1): lightVis,
                                          "translateX_%s" % (i + 1): tx,
                                          "translateY_%s" % (i + 1): ty,
                                          "translateZ_%s" % (i + 1): tz,
                                          "rotateX_%s" % (i + 1): rx,
                                          "rotateY_%s" % (i + 1): ry,
                                          "rotateZ_%s" % (i + 1): rz,
                                          "scaleX_%s" % (i + 1): sx,
                                          "scaleY_%s" % (i + 1): sy,
                                          "scaleZ_%s" % (i + 1): sz,
                                          "intensity_%s" % (i + 1): intensity,
                                          "exposure_%s" % (i + 1): exposure,
                                          "redColor_%s" % (i + 1): colR,
                                          "greenColor_%s" % (i + 1): colG,
                                          "blueColor_%s" % (i + 1): colB}
                        # print "CREATED DICTIONARY (test)"
                        dicExists = True

                    else:
                        self.setup_dic.update({"lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                               "lightType%s" % (i + 1): lightType,
                                               "visibility_%s" % (i + 1): vis,
                                               "mesh_visibility_%s" % (i + 1): showMesh,
                                               "light_visibility_%s" % (i + 1): lightVis,
                                               "translateX_%s" % (i + 1): tx,
                                               "translateY_%s" % (i + 1): ty,
                                               "translateZ_%s" % (i + 1): tz,
                                               "rotateX_%s" % (i + 1): rx,
                                               "rotateY_%s" % (i + 1): ry,
                                               "rotateZ_%s" % (i + 1): rz,
                                               "scaleX_%s" % (i + 1): sx,
                                               "scaleY_%s" % (i + 1): sy,
                                               "scaleZ_%s" % (i + 1): sz,
                                               "intensity_%s" % (i + 1): intensity,
                                               "exposure_%s" % (i + 1): exposure,
                                               "redColor_%s" % (i + 1): colR,
                                               "greenColor_%s" % (i + 1): colG,
                                               "blueColor_%s" % (i + 1): colB})
                        # print "DIC UPDATED (test)"

                    cmds.confirmDialog(title='Error', message="Mesh Lights can not be saved properly."
                                                              "Will just load an empty Mesh Light with the attributes"
                                                              "You can then copy those attributes to a new Mesh Light",
                                       button=['CONFIRM'])

                if lightType == 'aiPhotometricLight':  # GET ALL THE RELEVANT ATTRIBUTES FOR A PHOTOMETRIC LIGHT
                    vis = cmds.getAttr("%s.visibility" % selectedLights[i])
                    tx = cmds.getAttr("%s.translateX" % selectedLights[i])
                    ty = cmds.getAttr("%s.translateY" % selectedLights[i])
                    tz = cmds.getAttr("%s.translateZ" % selectedLights[i])
                    # print "Translations for object %s are X: %s, Y: %s, Z: %s" % (i, tx, ty, tz)

                    rx = cmds.getAttr("%s.rotateX" % selectedLights[i])
                    ry = cmds.getAttr("%s.rotateY" % selectedLights[i])
                    rz = cmds.getAttr("%s.rotateZ" % selectedLights[i])
                    # print "Rotations for object %s are X: %s, Y: %s, Z: %s" % (i, rx, ry, rz)

                    sx = cmds.getAttr("%s.scaleX" % selectedLights[i])
                    sy = cmds.getAttr("%s.scaleY" % selectedLights[i])
                    sz = cmds.getAttr("%s.scaleZ" % selectedLights[i])
                    # print "Scales for object %s are X: %s, Y: %s, Z: %s" % (i, sx, sy, sz)

                    intensity = cmds.getAttr("%s.intensity" % selectedLights[i])
                    exposure = cmds.getAttr("%s.exposure" % selectedLights[i])
                    colR = cmds.getAttr("%s.colorR" % selectedLights[i])
                    colG = cmds.getAttr("%s.colorG" % selectedLights[i])
                    colB = cmds.getAttr("%s.colorB" % selectedLights[i])

                    if i == 0:
                        self.setup_dic = {"numOfLights": amountOfLights,
                                          "lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                          "lightType_%s" % (i + 1): lightType,
                                          "visibility_%s" % (i + 1): vis,
                                          "translateX_%s" % (i + 1): tx,
                                          "translateY_%s" % (i + 1): ty,
                                          "translateZ_%s" % (i + 1): tz,
                                          "rotateX_%s" % (i + 1): rx,
                                          "rotateY_%s" % (i + 1): ry,
                                          "rotateZ_%s" % (i + 1): rz,
                                          "scaleX_%s" % (i + 1): sx,
                                          "scaleY_%s" % (i + 1): sy,
                                          "scaleZ_%s" % (i + 1): sz,
                                          "intensity_%s" % (i + 1): intensity,
                                          "exposure_%s" % (i + 1): exposure,
                                          "redColor_%s" % (i + 1): colR,
                                          "greenColor_%s" % (i + 1): colG,
                                          "blueColor_%s" % (i + 1): colB}
                        # print "CREATED DICTIONARY (test)"
                        dicExists = True

                    else:
                        self.setup_dic.update({"lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                               "lightType_%s" % (i + 1): lightType,
                                               "visibility_%s" % (i + 1): vis,
                                               "translateX_%s" % (i + 1): tx,
                                               "translateY_%s" % (i + 1): ty,
                                               "translateZ_%s" % (i + 1): tz,
                                               "rotateX_%s" % (i + 1): rx,
                                               "rotateY_%s" % (i + 1): ry,
                                               "rotateZ_%s" % (i + 1): rz,
                                               "scaleX_%s" % (i + 1): sx,
                                               "scaleY_%s" % (i + 1): sy,
                                               "scaleZ_%s" % (i + 1): sz,
                                               "intensity_%s" % (i + 1): intensity,
                                               "exposure_%s" % (i + 1): exposure,
                                               "redColor_%s" % (i + 1): colR,
                                               "greenColor_%s" % (i + 1): colG,
                                               "blueColor_%s" % (i + 1): colB})
                        # print "DIC UPDATED (test)"

                if lightType == 'aiSkyDomeLight':  # GET ALL THE RELEVANT ATTRIBUTES FOR A SKYDOME LIGHT
                    vis = cmds.getAttr("%s.visibility" % selectedLights[i])

                    tx = cmds.getAttr("%s.translateX" % selectedLights[i])
                    ty = cmds.getAttr("%s.translateY" % selectedLights[i])
                    tz = cmds.getAttr("%s.translateZ" % selectedLights[i])
                    # print "Translations for object %s are X: %s, Y: %s, Z: %s" % (i, tx, ty, tz)

                    rx = cmds.getAttr("%s.rotateX" % selectedLights[i])
                    ry = cmds.getAttr("%s.rotateY" % selectedLights[i])
                    rz = cmds.getAttr("%s.rotateZ" % selectedLights[i])
                    # print "Rotations for object %s are X: %s, Y: %s, Z: %s" % (i, rx, ry, rz)

                    sx = cmds.getAttr("%s.scaleX" % selectedLights[i])
                    sy = cmds.getAttr("%s.scaleY" % selectedLights[i])
                    sz = cmds.getAttr("%s.scaleZ" % selectedLights[i])
                    # print "Scales for object %s are X: %s, Y: %s, Z: %s" % (i, sx, sy, sz)

                    intensity = cmds.getAttr("%s.intensity" % selectedShapes[i])
                    exposure = cmds.getAttr("%s.exposure" % selectedShapes[i])
                    colR = cmds.getAttr("%s.colorR" % selectedLights[i])
                    colG = cmds.getAttr("%s.colorG" % selectedLights[i])
                    colB = cmds.getAttr("%s.colorB" % selectedLights[i])

                    sr = cmds.getAttr("%s.skyRadius" % selectedLights[i])

                    if i == 0:
                        self.setup_dic = {"numOfLights": amountOfLights,
                                          "lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                          "lightType_%s" % (i + 1): lightType,
                                          "visibility_%s" % (i + 1): vis,
                                          "translateX_%s" % (i + 1): tx,
                                          "translateY_%s" % (i + 1): ty,
                                          "translateZ_%s" % (i + 1): tz,
                                          "rotateX_%s" % (i + 1): rx,
                                          "rotateY_%s" % (i + 1): ry,
                                          "rotateZ_%s" % (i + 1): rz,
                                          "scaleX_%s" % (i + 1): sx,
                                          "scaleY_%s" % (i + 1): sy,
                                          "scaleZ_%s" % (i + 1): sz,
                                          "intensity_%s" % (i + 1): intensity,
                                          "exposure_%s" % (i + 1): exposure,
                                          "redColor_%s" % (i + 1): colR,
                                          "greenColor_%s" % (i + 1): colG,
                                          "blueColor_%s" % (i + 1): colB,
                                          "skyRadius_%s" % (i + 1): sr}
                        # print "CREATED DICTIONARY (test)"
                        dicExists = True

                    else:
                        self.setup_dic.update({"lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                               "lightType_%s" % (i + 1): lightType,
                                               "visibility_%s" % (i + 1): vis,
                                               "translateX_%s" % (i + 1): tx,
                                               "translateY_%s" % (i + 1): ty,
                                               "translateZ_%s" % (i + 1): tz,
                                               "rotateX_%s" % (i + 1): rx,
                                               "rotateY_%s" % (i + 1): ry,
                                               "rotateZ_%s" % (i + 1): rz,
                                               "scaleX_%s" % (i + 1): sx,
                                               "scaleY_%s" % (i + 1): sy,
                                               "scaleZ_%s" % (i + 1): sz,
                                               "intensity_%s" % (i + 1): intensity,
                                               "exposure_%s" % (i + 1): exposure,
                                               "redColor_%s" % (i + 1): colR,
                                               "greenColor_%s" % (i + 1): colG,
                                               "blueColor_%s" % (i + 1): colB,
                                               "skyRadius_%s" % (i + 1): sr})
                        # print "DIC UPDATED (test)"

                if lightType == 'aiLightPortal':  # GET ALL THE RELEVANT ATTRIBUTES FOR AN AREA LIGHT
                    vis = cmds.getAttr("%s.visibility" % selectedLights[i])
                    tx = cmds.getAttr("%s.translateX" % selectedLights[i])
                    ty = cmds.getAttr("%s.translateY" % selectedLights[i])
                    tz = cmds.getAttr("%s.translateZ" % selectedLights[i])
                    # print "Translations for object %s are X: %s, Y: %s, Z: %s" % (i, tx, ty, tz)

                    rx = cmds.getAttr("%s.rotateX" % selectedLights[i])
                    ry = cmds.getAttr("%s.rotateY" % selectedLights[i])
                    rz = cmds.getAttr("%s.rotateZ" % selectedLights[i])
                    # print "Rotations for object %s are X: %s, Y: %s, Z: %s" % (i, rx, ry, rz)

                    sx = cmds.getAttr("%s.scaleX" % selectedLights[i])
                    sy = cmds.getAttr("%s.scaleY" % selectedLights[i])
                    sz = cmds.getAttr("%s.scaleZ" % selectedLights[i])
                    # print "Scales for object %s are X: %s, Y: %s, Z: %s" % (i, sx, sy, sz)

                    if i == 0:
                        self.setup_dic = {"numOfLights": amountOfLights,
                                          "lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                          "lightType_%s" % (i + 1): lightType,
                                          "visibility_%s" % (i + 1): vis,
                                          "translateX_%s" % (i + 1): tx,
                                          "translateY_%s" % (i + 1): ty,
                                          "translateZ_%s" % (i + 1): tz,
                                          "rotateX_%s" % (i + 1): rx,
                                          "rotateY_%s" % (i + 1): ry,
                                          "rotateZ_%s" % (i + 1): rz,
                                          "scaleX_%s" % (i + 1): sx,
                                          "scaleY_%s" % (i + 1): sy,
                                          "scaleZ_%s" % (i + 1): sz}
                        # print "CREATED DICTIONARY (test)"
                        dicExists = True

                    else:
                        self.setup_dic.update({"lightName_%s" % (i + 1): "%s" % (selectedLights[i]),
                                               "lightType_%s" % (i + 1): lightType,
                                               "visibility_%s" % (i + 1): vis,
                                               "translateX_%s" % (i + 1): tx,
                                               "translateY_%s" % (i + 1): ty,
                                               "translateZ_%s" % (i + 1): tz,
                                               "rotateX_%s" % (i + 1): rx,
                                               "rotateY_%s" % (i + 1): ry,
                                               "rotateZ_%s" % (i + 1): rz,
                                               "scaleX_%s" % (i + 1): sx,
                                               "scaleY_%s" % (i + 1): sy,
                                               "scaleZ_%s" % (i + 1): sz})
                        # print "DIC UPDATED (test)"

        else:
            cmds.confirmDialog(title='Error', message="You need to have lights selected first",
                               button=['CONFIRM'])

    # Function that works as a vehicle to create save locations (is used by createPreviewPic and saveSetups functions
    def create_custom_save_location(self, folder_name, *args):  # CREATING SAVE LOCATION
        # this is so it is not hardcoded
        self.proj_dir = cmds.internalVar(userWorkspaceDir=True)
        self.new_dir = os.path.join(self.proj_dir, folder_name)
        if not os.path.exists(self.new_dir):
            os.makedirs(self.new_dir)
        # print "NEW DIRECTORY IS: %s" % self.new_dir
        return self.new_dir

    # Saves the setups into a .txt file
    def save_into_file(self, setupName, *args):
        file_name = "%s.txt" % setupName
        new_file = os.path.join(self.saveLocation, file_name)

        save_dic = self.setup_dic
        jSonString = json.dumps(save_dic)

        fileHandle = open(new_file, "w")
        fileHandle.write(jSonString)
        fileHandle.close()
        print "Saved into %s (test)" % file_name
        self.lightController()

    # Function that creates the preview setup to render out the thumbnails
    def spawnPreviewSetup(self, pSetupName,*args):
        setupName = cmds.textField(pSetupName, query=True, text=True)

        if cmds.objExists("PREVIEW_SETUP"):
            cmds.delete("PREVIEW_SETUP")

        group_name = "PREVIEW_SETUP"
        cmds.group(n=group_name, empty=True)

        infiniName = "Preview_INFINI"
        placeHolderName = "placeHolderName"
        cmds.polyPlane(w=5500, h=1600, sx=1, sy=1)
        cmds.rename(placeHolderName)
        cmds.polyExtrudeEdge('%s.e[3]' % placeHolderName, tz=-500)
        cmds.polyExtrudeEdge('%s.e[6]' % placeHolderName, ty=500)
        cmds.polyExtrudeEdge('%s.e[9]' % placeHolderName, ty=725)
        cmds.displaySmoothness(placeHolderName, du=3, dv=5, pw=16, ps=4, po=3)
        cmds.parent(placeHolderName, group_name)
        cmds.rename(infiniName)
        cmds.select(clear=True)

        sphereName = "Preview_SPHERE"
        sphereSize = 88
        cmds.polySphere(name=sphereName)
        cmds.move(0, sphereSize, 0)
        cmds.scale(sphereSize, sphereSize, sphereSize)
        cmds.displaySmoothness(sphereName, du=3, dv=5, pw=16, ps=4, po=3)
        cmds.rename(placeHolderName)
        cmds.parent(placeHolderName, group_name)
        cmds.rename(sphereName)

        camName = "Preview_CAM"
        cmds.camera()
        camShape = "%sShape" % camName

        cmds.move(0, sphereSize, sphereSize*4)
        cmds.scale(sphereSize*0.4, sphereSize*0.4, sphereSize*0.4)
        cmds.rename(placeHolderName)
        cmds.parent(placeHolderName, group_name)
        cmds.rename(camName)
        cmds.setAttr("%s.horizontalFilmAperture" % camShape, 1.417)
        cmds.setAttr("%s.verticalFilmAperture" % camShape, 0.945)
        cmds.setAttr("%s.focalLength" % camShape, 50)
        cmds.setAttr("%s.lensSqueezeRatio" % camShape, 1)
        cmds.setAttr("%s.fStop" % camShape, 5.6)
        cmds.setAttr("%s.focusDistance" % camShape, 5)
        cmds.setAttr("%s.shutterAngle" % camShape, 144)
        cmds.setAttr("%s.centerOfInterest" % camShape, 20.837)

        self.createPreviewPic(camName, setupName)

    # Function that renders out and saves the thumbnails in the "previewPics" folder
    def createPreviewPic(self, pRenderCam, pSetupName, *args):
        # print "CREATE PREVIEW PIC CALLED (test)"
        arnoldRender(1, 1, True, True, pRenderCam, ' -layer defaultRenderLayer')

        cmds.setAttr("defaultArnoldDriver.ai_translator", "jpeg", type="string")
        arnoldRender(260, 260, True, True, pRenderCam, ' -layer defaultRenderLayer')

        editor = 'renderView'
        formatManager = createImageFormats.ImageFormats()
        formatManager.pushRenderGlobalsForDesc("JPEG")
        cmds.renderWindowEditor(editor, e=True,
                                writeImage='%s\%s_pp.jpg' % (self.thumbnailsLocation, pSetupName))
        formatManager.popRenderGlobals()

        self.presetButtons()
        self.lightController()

    # Function that is the vehicle to load any setup, is used by the buttons and by the loadAnyPreset function
    def load_setups(self, pFileName, *args):
        file_name = pFileName
        load_file = os.path.join(self.saveLocation, file_name)

        if os.path.exists(load_file):
            file_handle = open(load_file, "r")
            file_string = file_handle.read()
            self.load_dic = json.loads(file_string)

            diffName = file_name.replace('.txt', '')
            groupName = "%s_lightGroup" % diffName
            amountOfLights = self.load_dic["numOfLights"]

            if not cmds.objExists(groupName):
                cmds.group(name=groupName, em=True)

                # LOOPS OVER EVERY LIGHT
                for i in range(amountOfLights):
                    lightName = "%s" % self.load_dic["lightName_%s" % (i+1)]
                    # print "Light Name: %s (test)" % lightName
                    lightType = "%s" % self.load_dic["lightType_%s" % (i+1)]
                    # print "Lighttype: %s (test)" % lightType

                    if lightType == 'aiAreaLight':
                        tx = self.load_dic["translateX_%s" % (i+1)]
                        ty = self.load_dic["translateY_%s" % (i+1)]
                        tz = self.load_dic["translateZ_%s" % (i+1)]

                        sx = self.load_dic["scaleX_%s" % (i+1)]
                        sy = self.load_dic["scaleY_%s" % (i+1)]
                        sz = self.load_dic["scaleZ_%s" % (i+1)]

                        rx = self.load_dic["rotateX_%s" % (i+1)]
                        ry = self.load_dic["rotateY_%s" % (i+1)]
                        rz = self.load_dic["rotateZ_%s" % (i+1)]

                        intensity = self.load_dic["intensity_%s" % (i+1)]
                        exposure = self.load_dic["exposure_%s" % (i+1)]

                        softEdge = self.load_dic["softEdge_%s" % (i+1)]
                        spread = self.load_dic["spread_%s" % (i+1)]
                        roundness = self.load_dic["roundness_%s" % (i+1)]

                        colR = self.load_dic["redColor_%s" % (i+1)]
                        colG = self.load_dic["greenColor_%s" % (i+1)]
                        colB = self.load_dic["blueColor_%s" % (i+1)]

                        placeHolderName = "placeholderName_%s" % i

                        mutils.createLocator(lightType, asLight=True)
                        cmds.rename(placeHolderName)  # rename it to a random name

                        cmds.move(tx, ty, tz)
                        cmds.scale(sx, sy, sz)
                        cmds.rotate(rx, ry, rz)

                        cmds.setAttr(("%s.intensity" % placeHolderName), intensity)
                        cmds.setAttr(("%s.exposure" % placeHolderName), exposure)

                        cmds.setAttr(("%s.aiSoftEdge" % placeHolderName), softEdge)
                        cmds.setAttr(("%s.aiSpread" % placeHolderName), spread)
                        cmds.setAttr(("%s.aiRoundness" % placeHolderName), roundness)

                        cmds.setAttr(("%s.colorR" % placeHolderName), colR)
                        cmds.setAttr(("%s.colorG" % placeHolderName), colG)
                        cmds.setAttr(("%s.colorB" % placeHolderName), colB)

                        cmds.parent(placeHolderName, groupName)  # parent it
                        cmds.rename(lightName)  # (because of this method we can have duplicate names if wanted)

                    if lightType == 'aiMeshLight':
                        vis = self.load_dic["visibility_%s" % (i+1)]
                        showMesh = self.load_dic["mesh_visibility_%s" % (i+1)]
                        lightVis = self.load_dic["light_visibility_%s" % (i + 1)]

                        tx = self.load_dic["translateX_%s" % (i+1)]
                        ty = self.load_dic["translateY_%s" % (i+1)]
                        tz = self.load_dic["translateZ_%s" % (i+1)]

                        sx = self.load_dic["scaleX_%s" % (i+1)]
                        sy = self.load_dic["scaleY_%s" % (i+1)]
                        sz = self.load_dic["scaleZ_%s" % (i+1)]

                        rx = self.load_dic["rotateX_%s" % (i+1)]
                        ry = self.load_dic["rotateY_%s" % (i+1)]
                        rz = self.load_dic["rotateZ_%s" % (i+1)]

                        intensity = self.load_dic["intensity_%s" % (i+1)]
                        exposure = self.load_dic["exposure_%s" % (i+1)]

                        colR = self.load_dic["redColor_%s" % (i+1)]
                        colG = self.load_dic["greenColor_%s" % (i+1)]
                        colB = self.load_dic["blueColor_%s" % (i+1)]

                        placeHolderName = "placeholderName_%s" % i

                        mutils.createLocator(lightType, asLight=True)
                        cmds.rename(placeHolderName)  # rename it to a random name

                        cmds.move(tx, ty, tz)
                        cmds.scale(sx, sy, sz)
                        cmds.rotate(rx, ry, rz)

                        cmds.setAttr(("%sShape.intensity" % placeHolderName), intensity)
                        cmds.setAttr(("%sShape.aiExposure" % placeHolderName), exposure)
                        cmds.setAttr(("%s.visibility" % placeHolderName), vis)
                        cmds.setAttr(("%s.showOriginalMesh" % placeHolderName), showMesh)
                        cmds.setAttr(("%s.lightVisible" % placeHolderName), lightVis)

                        cmds.setAttr(("%s.colorR" % placeHolderName), colR)
                        cmds.setAttr(("%s.colorG" % placeHolderName), colG)
                        cmds.setAttr(("%s.colorB" % placeHolderName), colB)

                        cmds.parent(placeHolderName, groupName)  # parent it
                        cmds.rename(lightName)  # (because of this method we can have duplicate names if wanted)

                        cmds.confirmDialog(title='Error',
                                           message="Mesh Lights can not be saved properly."
                                                   "Will just load an empty Mesh Light with the attributes"
                                                   "You can then copy those attributes to a new Mesh Light",
                                           button=['CONFIRM'])

                    if lightType == 'aiPhotometricLight':
                        vis = self.load_dic["visibility_%s" % (i + 1)]

                        tx = self.load_dic["translateX_%s" % (i + 1)]
                        ty = self.load_dic["translateY_%s" % (i + 1)]
                        tz = self.load_dic["translateZ_%s" % (i + 1)]

                        sx = self.load_dic["scaleX_%s" % (i + 1)]
                        sy = self.load_dic["scaleY_%s" % (i + 1)]
                        sz = self.load_dic["scaleZ_%s" % (i + 1)]

                        rx = self.load_dic["rotateX_%s" % (i + 1)]
                        ry = self.load_dic["rotateY_%s" % (i + 1)]
                        rz = self.load_dic["rotateZ_%s" % (i + 1)]

                        intensity = self.load_dic["intensity_%s" % (i + 1)]
                        exposure = self.load_dic["exposure_%s" % (i + 1)]

                        colR = self.load_dic["redColor_%s" % (i + 1)]
                        colG = self.load_dic["greenColor_%s" % (i + 1)]
                        colB = self.load_dic["blueColor_%s" % (i + 1)]

                        placeHolderName = "placeholderName_%s" % i

                        mutils.createLocator(lightType, asLight=True)
                        cmds.rename(placeHolderName)  # rename it to a random name

                        cmds.move(tx, ty, tz)
                        cmds.scale(sx, sy, sz)
                        cmds.rotate(rx, ry, rz)

                        cmds.setAttr(("%sShape.intensity" % placeHolderName), intensity)
                        cmds.setAttr(("%sShape.exposure" % placeHolderName), exposure)
                        cmds.setAttr(("%s.visibility" % placeHolderName), vis)

                        cmds.setAttr(("%s.colorR" % placeHolderName), colR)
                        cmds.setAttr(("%s.colorG" % placeHolderName), colG)
                        cmds.setAttr(("%s.colorB" % placeHolderName), colB)

                        cmds.parent(placeHolderName, groupName)  # parent it
                        cmds.rename(lightName)  # (because of this method we can have duplicate names if wanted)

                    if lightType == 'aiSkyDomeLight':
                        vis = self.load_dic["visibility_%s" % (i + 1)]

                        tx = self.load_dic["translateX_%s" % (i + 1)]
                        ty = self.load_dic["translateY_%s" % (i + 1)]
                        tz = self.load_dic["translateZ_%s" % (i + 1)]

                        sx = self.load_dic["scaleX_%s" % (i + 1)]
                        sy = self.load_dic["scaleY_%s" % (i + 1)]
                        sz = self.load_dic["scaleZ_%s" % (i + 1)]

                        rx = self.load_dic["rotateX_%s" % (i + 1)]
                        ry = self.load_dic["rotateY_%s" % (i + 1)]
                        rz = self.load_dic["rotateZ_%s" % (i + 1)]

                        intensity = self.load_dic["intensity_%s" % (i + 1)]
                        print "INTENSITY: %s" % intensity

                        exposure = self.load_dic["exposure_%s" % (i + 1)]
                        print "EXPOSURE: %s" % exposure

                        colR = self.load_dic["redColor_%s" % (i + 1)]
                        colG = self.load_dic["greenColor_%s" % (i + 1)]
                        colB = self.load_dic["blueColor_%s" % (i + 1)]

                        skyRadius = self.load_dic["skyRadius_%s" % (i + 1)]

                        placeHolderName = "placeholderName_%s" % i

                        mutils.createLocator(lightType, asLight=True)
                        cmds.rename(placeHolderName)  # rename it to a random name

                        cmds.move(tx, ty, tz)
                        cmds.scale(sx, sy, sz)
                        cmds.rotate(rx, ry, rz)

                        cmds.setAttr(("%s.intensity" % placeHolderName), intensity)
                        cmds.setAttr(("%s.exposure" % placeHolderName), exposure)
                        cmds.setAttr(("%s.visibility" % placeHolderName), vis)

                        cmds.setAttr(("%s.colorR" % placeHolderName), colR)
                        cmds.setAttr(("%s.colorG" % placeHolderName), colG)
                        cmds.setAttr(("%s.colorB" % placeHolderName), colB)

                        cmds.setAttr(("%s.skyRadius" % placeHolderName), skyRadius)

                        cmds.parent(placeHolderName, groupName)  # parent it
                        cmds.rename(lightName)  # (because of this method we can have duplicate names if wanted)

                    if lightType == 'aiLightPortal':
                        vis = self.load_dic["visibility_%s" % (i + 1)]

                        tx = self.load_dic["translateX_%s" % (i + 1)]
                        ty = self.load_dic["translateY_%s" % (i + 1)]
                        tz = self.load_dic["translateZ_%s" % (i + 1)]

                        sx = self.load_dic["scaleX_%s" % (i + 1)]
                        sy = self.load_dic["scaleY_%s" % (i + 1)]
                        sz = self.load_dic["scaleZ_%s" % (i + 1)]

                        rx = self.load_dic["rotateX_%s" % (i + 1)]
                        ry = self.load_dic["rotateY_%s" % (i + 1)]
                        rz = self.load_dic["rotateZ_%s" % (i + 1)]

                        placeHolderName = "placeholderName_%s" % i

                        mutils.createLocator(lightType, asLight=True)
                        cmds.rename(placeHolderName)  # rename it to a random name

                        cmds.move(tx, ty, tz)
                        cmds.scale(sx, sy, sz)
                        cmds.rotate(rx, ry, rz)

                        cmds.setAttr(("%s.visibility" % lightName), vis)

                        cmds.parent(placeHolderName, groupName)  # parent it
                        cmds.rename(lightName)  # (because of this method we can have duplicate names if wanted)

                    if lightType == 'aiPhotometricLight':
                        vis = self.load_dic["visibility_%s" % (i + 1)]

                        tx = self.load_dic["translateX_%s" % (i + 1)]
                        ty = self.load_dic["translateY_%s" % (i + 1)]
                        tz = self.load_dic["translateZ_%s" % (i + 1)]

                        sx = self.load_dic["scaleX_%s" % (i + 1)]
                        sy = self.load_dic["scaleY_%s" % (i + 1)]
                        sz = self.load_dic["scaleZ_%s" % (i + 1)]

                        rx = self.load_dic["rotateX_%s" % (i + 1)]
                        ry = self.load_dic["rotateY_%s" % (i + 1)]
                        rz = self.load_dic["rotateZ_%s" % (i + 1)]

                        intensity = self.load_dic["intensity_%s" % (i + 1)]
                        exposure = self.load_dic["exposure_%s" % (i + 1)]

                        colR = self.load_dic["redColor_%s" % (i + 1)]
                        colG = self.load_dic["greenColor_%s" % (i + 1)]
                        colB = self.load_dic["blueColor_%s" % (i + 1)]

                        placeHolderName = "placeholderName_%s" % i

                        mutils.createLocator(lightType, asLight=True)
                        cmds.rename(placeHolderName)  # rename it to a random name

                        cmds.move(tx, ty, tz)
                        cmds.scale(sx, sy, sz)
                        cmds.rotate(rx, ry, rz)

                        cmds.setAttr(("%s.intensity" % placeHolderName), intensity)
                        cmds.setAttr(("%s.exposure" % placeHolderName), exposure)
                        cmds.setAttr(("%s.visibility" % placeHolderName), vis)

                        cmds.setAttr(("%s.colorR" % placeHolderName), colR)
                        cmds.setAttr(("%s.colorG" % placeHolderName), colG)
                        cmds.setAttr(("%s.colorB" % placeHolderName), colB)

                        cmds.parent(placeHolderName, groupName)  # parent it
                        cmds.rename(lightName)  # (because of this method we can have duplicate names if wanted)

                self.lightController()

            else:
                cmds.confirmDialog(title='Error', message="You already have this setup present in your scene",
                                   button=['CONFIRM'])
        else:
            print "File don't exist"

        self.lightController()


winUI = DynaLight()
winUI.showUI()
